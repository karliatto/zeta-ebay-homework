'use strict';

module.exports = function(server) {
  server.middleware = {};

  // The order here is important.
  require('./parsers')(server);
  require('./security')(server);
};
