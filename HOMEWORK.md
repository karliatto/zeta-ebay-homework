Build a Node.js web app that sends an address form to the client. 

The address form should have the following fields:
    1. Name
    2. Address
    3. Country
    4. State / province
    5. Postal code
    6. Email
    7. Phone number

The address form should update states / provinces for each country selection. There could be N number of countries added to the list, so province / state data should be API driven. The email, phone number, and postal code fields also should have validators that can change when the user selects a different country. 

Guidance
 
We wouldn't ask anyone to do a coding puzzle that we haven't done ourselves, so the people looking at your code understand the problem we're asking to be solved. We’d like to see that you have considered the following:
 
    • Is your solution production ready?
    • Is your code modular and extendable?
    • Do you use modern JavaScript features and practices?
    • We are keen to see how much you think is enough, and how much would go into a Minimum Viable Product.  As a guide, elegant and simple wins over feature rich every time, though extra gold stars are given to people who get excited and do more because they are having fun.
    • Do you test your code? We'll also be looking for things like coverage, copy and paste detection etc.
    • Is your UI layer neatly separated from your business logic?
    • Please specify which is your target browser
    • Bonus points for cross-browser support
    • If it works on a mobile web, that would be even better!
    • Do you have documentation and run/build instructions?
