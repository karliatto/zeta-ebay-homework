import React from 'react';
import Layout from '../components/Layout';
import Form from '../components/Form';

export default () => (
  <Layout>
    <h1>Homework</h1>
    <Form />
  </Layout>
);
