'use strict';

const _ = require('lodash');
const States = require('../../data/states');

module.exports = function(server) {
  server.get('/api/states/', (req, res) => {
    const states = _.map(States, state => _.pick(state, 'id', 'label'));
    return res.status(200).json(states);
  });
};
