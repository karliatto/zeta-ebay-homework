'use strict';

module.exports = function(server) {
  server.post('/api/address', (req, res) => {
    const address = req.body;
    /**
     * This address object can be used for example to save it in a database.
     */
    return res.status(200).json({ address });
  });
};
