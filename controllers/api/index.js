'use strict';

module.exports = function(server) {
  require('./states')(server);
  require('./provinces')(server);
  require('./address')(server);
};
