'use strict';

const _ = require('lodash');
const States = require('../../data/states');

module.exports = function(server) {
  server.get('/api/provinces/:stateId', (req, res) => {
    const stateId = req.params.stateId;
    const data = _.chain(States)
      .find(state => state.id === stateId)
      .pick('provinces')
      .value();
    return res.status(200).json(data);
  });
};
