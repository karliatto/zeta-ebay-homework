# Node.js web app that sends an address form to the client.

## Consideartions

- Target browser = all browsers

## Development

Run it using the command below:

```bash
npm run dev
```

## Production

To run it for producction use the command below:

```
npm run build
npm start
```
