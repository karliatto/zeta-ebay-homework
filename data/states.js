module.exports = states = [
  {
    id: 'es',
    label: 'Spain',
    provinces: [
      {
        id: 'mad',
        label: 'Madrid',
      },
      {
        id: 'val',
        label: 'Valencia',
      },
    ],
  },
  {
    id: 'cz',
    label: 'Czech Republic',
    provinces: [
      {
        id: 'mor',
        label: 'Moravia',
      },
      {
        id: 'boh',
        label: 'Bohemia',
      },
    ],
  },
];
