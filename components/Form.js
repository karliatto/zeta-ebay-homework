import React, { Component } from 'react';
import Input from './Input';
import Select from './Select';
import _ from 'lodash';

class Form extends Component {
  constructor() {
    super();
    this.state = {
      countries: [],
      provinces: [],
      formData: {},
    };
  }

  componentDidMount() {
    fetch(`http://localhost:3000/api/states`)
      .then(response => response.json())
      .then(countries => {
        this.setState({
          countries,
        });
      })
      .catch(error => console.error(error));
  }

  fetchProvinces = stateId => {
    fetch(`http://localhost:3000/api/provinces/${stateId}`)
      .then(response => response.json())
      .then(data => {
        this.setState({ provinces: data.provinces });
      })
      .catch(error => console.error(error));
  };

  handleCountryChange = event => {
    event.preventDefault();
    const selectedCountry = event.target.value;
    this.fetchProvinces(selectedCountry);
    this.handleChange(event);
  };

  handleChange = event => {
    event.preventDefault();
    const formData = Object.assign({}, this.state.formData, {
      [event.target.name]: event.target.value,
    });
    this.setState({
      formData,
    });
  };

  onSubmit = event => {
    event.preventDefault();
    const formData = this.state.formData;

    fetch('http://localhost:3000/api/address', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'post',
      body: JSON.stringify(formData),
    })
      .then(function(response) {
        return response.json();
      })
      .catch(error => console.error(error));
  };

  style = {
    formGroup: { display: 'flex', flexDirection: 'column', width: '300px' },
    formControl: { display: 'flex', flexDirection: 'column' },
  };

  render() {
    return (
      <form style={this.style.formGroup}>
        <div style={this.style.formControl}>
          <label>Name</label>
          <Input name="name" onChange={this.handleChange} />
        </div>

        <div style={this.style.formControl}>
          <label>Address</label>
          <Input name="address" onChange={this.handleChange} />
        </div>

        <div style={this.style.formControl}>
          <label>Country</label>
          <Select
            name="country"
            items={this.state.countries}
            onChange={this.handleCountryChange}
          />
        </div>

        <div style={this.style.formControl}>
          <label>State / province</label>
          <Select
            name="province"
            items={this.state.provinces}
            onChange={this.handleChange}
          />
        </div>

        <div style={this.style.formControl}>
          <label>Postal code</label>
          <Input name="postalCode" onChange={this.handleChange} />
        </div>
        <div style={this.style.formControl}>
          <label>Email</label>
          <Input name="email" onChange={this.handleChange} />
        </div>

        <div style={this.style.formControl}>
          <label>Phone number</label>
          <Input name="phoneNumber" onChange={this.handleChange} />
        </div>

        <div style={this.style.formControl}>
          <button type="submit" onClick={this.onSubmit}>
            Submit
          </button>
        </div>
      </form>
    );
  }
}

export default Form;
