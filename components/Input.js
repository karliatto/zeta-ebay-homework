import React from 'react';

const Input = ({ name, onChange }) => {
  return <input type="text" name={name} onChange={onChange} />;
};

export default Input;
