import React from 'react';

class Select extends React.Component {
  constructor() {
    super();
  }

  getOptionItems = items => {
    if (!items) {
      return null;
    }
    return items.map(item => (
      <option key={item.id} value={item.id}>
        {item.label}
      </option>
    ));
  };

  render() {
    const { items, name, onChange } = this.props;
    let optionItems = this.getOptionItems(items);

    return (
      <div>
        <select name={name} onChange={onChange}>
          <option>Select</option>
          {optionItems}
        </select>
      </div>
    );
  }
}

export default Select;
